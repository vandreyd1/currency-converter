package com;

import java.io.*;
import java.math.*;
import java.util.*;
import java.util.Map.*;

public class MainApp {

  private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

  public static void main(String[] args) throws IOException {
    System.out.println("#############################");
    System.out.println("Input 'exit' to close the app");
    System.out.println("#############################");
    System.out.println(
        "Input 1 for testing standard converter or input 2 "
            + "for testing interaction with goods in the database");
    String input = reader.readLine().trim();
    exiting(input);
    while (!input.equals("1") || !input.equals("2")) {
      if ("1".equals(input)) {
        standardConverter();
        System.out.println();
      } else if ("2".equals(input)) {
        productPriceConverter();
        System.out.println();
      }
      System.out.println("Please input 1, 2 or exit");
      input = reader.readLine().trim();
      exiting(input);
    }
  }

  /**
   * Main converter functionality
   */
  private static void standardConverter() {
    System.out.println("Product price in USD: ");
    BigDecimal price;
    while ((price = checkedPrice()) == null) {
      System.out.println("Please enter a valid number");
      System.out.println("Product price in USD: ");
    }
    System.out.println("New currency: ");
    String currency;
    while ((currency = checkedCurrency()) == null) {
      System.out.println("Please enter one of the following currencies: UAH, EUR, GBP");
      System.out.println("New currency: ");
    }

    System.out.println("Converting\nPlease wait...");
    try {
      CurrencyConverter currencyConverter = new CurrencyConverter();
      BigDecimal convertedPrice = currencyConverter.getConvertedPrice(price, currency);
      if (convertedPrice != null) {
        System.out.println("Price in USD: " + price);
        System.out.println("Price in " + currency.toUpperCase() + ": " + convertedPrice);
      }
    } catch (IOException e) {
      System.err.println("Connection error: ");
      System.err.println(e.toString());
    }
  }

  private static BigDecimal checkedPrice() {
    try {
      String input = reader.readLine().replace(',', '.');
      exiting(input);
      return new BigDecimal(input);
    } catch (NumberFormatException | IOException e) {
      return null;
    }
  }

  private static String checkedCurrency() {
    try {
      String inputString = (reader.readLine()).trim().toUpperCase();
      exiting(inputString.toLowerCase());
      switch (inputString) {
        case "UAH":
        case "EUR":
        case "GBP":
          return inputString;
        default:
          return null;
      }
    } catch (IOException e) {
      return null;
    }
  }

  private static void exiting(String input) {
    if ((input.trim()).equals("exit")) {
      System.exit(0);
    }
  }

  /**
   * prints Product instance with converted prices Testing adding new prices for new currencies into
   * future database
   */
  private static void productPriceConverter() {
    List<String> currencies = Arrays.asList("UAH", "EUR", "GBP");
    Product product = new Product(1, "TV", new BigDecimal("523"), currencies);
    try {
      CurrencyConverter currencyProductConverter = new CurrencyConverter();
      BigDecimal primaryPrice = product.getPrimaryPriceUSD();
      Map<String, BigDecimal> secondaryPrices = product.getSecondaryPrices();
      for (Entry<String, BigDecimal> prices : secondaryPrices.entrySet()) {
        BigDecimal convertedPrice = currencyProductConverter
            .getConvertedPrice(primaryPrice, prices.getKey());
        if (convertedPrice != null) {
          prices.setValue(convertedPrice);
        } else {
          System.out.println("Conversion error for currency: " + prices.getKey());
        }
      }
    } catch (IOException e) {
      System.err.println("Connection error: ");
      System.err.println(e.toString());
    }
    System.out.println(product.toString());
  }
}



