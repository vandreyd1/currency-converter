package com;

import com.google.gson.*;
import java.math.*;
import java.util.*;

/**
 * This class should be a database entity in future
 */
public class Product {

  private int id;
  private String name;
  private BigDecimal primaryPriceUSD;
  private Map<String, BigDecimal> secondaryPrices;


  public Product() {
  }

  /**
   * Just for testing this constructor is used to create an instance with name, primary price in usd
   * and a set of secondary currencies
   */
  public Product(int id, String name, BigDecimal primaryPriceUSD, List<String> secondaryCurrencies) {
    this.id = id;
    this.name = name;
    this.primaryPriceUSD = primaryPriceUSD;
    this.secondaryPrices = new HashMap<>();
    for (String currency : secondaryCurrencies) {
      secondaryPrices.put(currency, null);
    }
  }

  public BigDecimal getPrimaryPriceUSD() {
    return primaryPriceUSD;
  }

  public void setPrimaryPriceUSD(BigDecimal primaryPriceUSD) {
    this.primaryPriceUSD = primaryPriceUSD;
  }

  public Map<String, BigDecimal> getSecondaryPrices() {
    return secondaryPrices;
  }

  public void setSecondaryPrices(Map<String, BigDecimal> secondaryPrices) {
    this.secondaryPrices = secondaryPrices;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Product)) {
      return false;
    }
    Product product = (Product) o;
    return id == product.id &&
        Objects.equals(name, product.name);
  }

  @Override
  public int hashCode() {

    return Objects.hash(id, name);
  }

  @Override
  public String toString() {
    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    return gson.toJson(this);
  }
}
