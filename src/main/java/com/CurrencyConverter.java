package com;

import com.google.gson.*;
import java.io.*;
import java.math.*;
import java.net.*;

public class CurrencyConverter {

  private final String apiUrl = "https://v3.exchangerate-api.com/bulk/89dba9b5155cead1951492eb/USD";
  private JsonObject responseResult;

  CurrencyConverter() throws IOException {
    URL url = new URL(apiUrl);
    HttpURLConnection request = (HttpURLConnection) url.openConnection();
    request.connect();
    JsonParser jp = new JsonParser();
    JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
    this.responseResult = root.getAsJsonObject();
  }

  public BigDecimal getConvertedPrice(BigDecimal priceUSD, String currency) {
    String resultResponseStatus = responseResult.get("result").toString();

    BigDecimal result = null;
    if (resultResponseStatus.equals("\"" + "success" + "\"")) {
      JsonObject rates = responseResult.getAsJsonObject("rates");
      BigDecimal currencyRate = new BigDecimal(rates.get(currency).toString());
      result = (priceUSD.multiply(currencyRate).setScale(2, RoundingMode.HALF_UP));
    } else {
      System.err.println("Error:");
      System.err.println(responseResult.get("error").toString());
    }
    return result;
  }
}
