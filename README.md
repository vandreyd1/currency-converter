# Task
* Write a function of the conversion price for a product in one of three currencies: UAH, EUR, GBP. 
* The function takes the price in USD and the currency into which to convert (string type).
* The function returns the price in the desired currency and / or an error. 
* The function must request the current exchange rate from a third-party service

# Description
* You must have [JRE](http://www.oracle.com/technetwork/java/javase/downloads/index.html) installed to run the application.
* The application uses the third-party API [www.exchangerate-api.com](https://www.exchangerate-api.com/)
* To demonstrate the conversion function, you need to choose one of the following modes:
  1) App will ask you to input the price in USD and the currency into which the price will be converted
  2) Demo of the conversion of prices for goods
* You can input 'exit' to close the app
